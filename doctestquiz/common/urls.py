from django.contrib import admin
from django.urls import path
from snippets.views import SnippetView

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", SnippetView.as_view(), name="runcode"),
]
