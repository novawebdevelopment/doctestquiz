import epicbox
from django import forms
from activities.models import Activity, Doctest


epicbox.configure(profiles=[epicbox.Profile("python", "python:3.8.5-alpine")])
limits = {"cputime": 3, "realtime": 10, "memory": 16, "processes": 1}


class ActivityRunnerForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = "__all__"
        labels = {"source_code": "Source Code"}


class ActivityEditorForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = "__all__"
        widgets = {"function_header": forms.TextInput(attrs={"placeholder": "foo(bar=None)"})}

    def clean_attempts(self):
        if self.cleaned_data["attempts"] < 1 and self.cleaned_data["attempts"] != -1:
            raise forms.ValidationError("The number of attempts is invalid.")
        return self.cleaned_data["attempts"]


class DoctestForm(forms.ModelForm):
    class Meta:
        model = Doctest
        fields = "__all__"
        widgets = {
            "input": forms.TextInput(attrs={"placeholder": ">>> foo()"}),
            "output": forms.TextInput(attrs={"placeholder": "'bar'"}),
        }


DoctestsFormSet = forms.inlineformset_factory(Activity, Doctest, form=DoctestForm, extra=0, min_num=1)