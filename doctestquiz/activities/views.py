from django.views import View
from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponseRedirect
from common.mixins import NextPageMixin
from activities.forms import ActivityEditorForm, ActivityRunnerForm, DoctestsFormSet
from activities.models import Activity, Doctest


class ListView(View):
    def get(self, request):
        activities = Activity.objects.filter(is_hidden=False)
        return render(request=request, template_name="activities/list.html", context={"activities": activities})


class ActivityRunnerView(View):
    def get(self, request):
        form = ActivityRunnerForm()
        return render(request=request, template_name="activities/runner.html", context={"form": form})

    def post(self, request):
        epicbox.configure(profiles=[epicbox.Profile("python", "python:3.8.5-alpine")])

        form = ActivityRunnerForm(request.POST)
        if form.is_valid():
            content = form.cleaned_data["source_code"]
            content += "\nimport doctest\ndoctest.testmod()"

            limits = {"cputime": 3, "realtime": 10, "memory": 16, "processes": 1}
            files = [{"name": "main.py", "content": content.encode("utf-8")}]
            result = epicbox.run("python", "python3 main.py", files=files, limits=limits)

            status = ""

            if result["exit_code"] != 0:
                if result["timeout"]:
                    status += "The runner has timed out. Are you entering an infinite loop?\n\n"
                elif result["oom_killed"]:
                    status += "The runner has run out of memory. Are you creating too many objects?\n\n"
                else:
                    status += "Check the output for errors.\n\n"

                status += "Exit code: " + str(result["exit_code"]) + "\n"
                output = result["stderr"].decode("utf-8")
                color = "danger"
            else:
                status += "Success!" + "\n\n"
                status += "Exit code: " + str(result["exit_code"]) + "\n"
                output = result["stdout"].decode("utf-8")
                color = "success"

            status += "Total execution time: " + str(result["duration"])

            return render(
                request=request,
                template_name="activities/runner.html",
                context={"form": form, "output": output, "status": status, "color": color},
            )
        return render(request=request, template_name="activities/runner.html", context={"form": form})


class ActivityEditorView(NextPageMixin, View):
    def get(self, request, uid=None):
        if uid is None:
            form = ActivityEditorForm()
            formset = DoctestsFormSet()
        else:
            try:
                form = ActivityEditorForm(instance=Activity.objects.get(uid=uid))
                formset = DoctestsFormSet(instance=Activity.objects.get(uid=uid))
            except (TypeError, ValueError, OverflowError, Activity.DoesNotExist):
                messages.add_message(request, messages.ERROR, "The activity doesn't exist.")
                return HttpResponseRedirect("/")
        return render(
            request=request, template_name="activities/editor.html", context={"form": form, "formset": formset}
        )

    def post(self, request, uid=None):
        if uid is None:
            form = ActivityEditorForm(request.POST)
            formset = DoctestsFormSet(request.POST)
            if form.is_valid():
                activity = form.save(commit=False)
                formset = DoctestsFormSet(request.POST, instance=activity)
                if formset.is_valid():
                    activity.save()
                    form.save_m2m()
                    formset.save()
                    messages.add_message(request, messages.SUCCESS, "The activity has been successfully created.")
                    return HttpResponseRedirect(self.next)
        else:
            try:
                form = ActivityEditorForm(request.POST, instance=Activity.objects.get(uid=uid))
                formset = DoctestsFormSet(request.POST, instance=Activity.objects.get(uid=uid))
                if form.is_valid() and formset.is_valid():
                    form.save()
                    formset.save()
                    messages.add_message(request, messages.SUCCESS, "The activity has been successfully edited.")
                    return HttpResponseRedirect(self.next)
            except (TypeError, ValueError, OverflowError, Activity.DoesNotExist):
                messages.add_message(request, messages.ERROR, "The activity doesn't exist.")
                return HttpResponseRedirect("/")
        return render(
            request=request, template_name="activities/editor.html", context={"form": form, "formset": formset}
        )
