from django.urls import path
from activities import views

app_name = "activities"

urlpatterns = [
    path("runner/", views.ActivityRunnerView.as_view(), name="runner"),
    path("editor/", views.ActivityEditorView.as_view(), name="editor"),
    path("<int:uid>/editor/", views.ActivityEditorView.as_view(), name="editor"),
]
