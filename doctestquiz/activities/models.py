from django.db import models
from accounts.models import Account


class Activity(models.Model):
    uid = models.AutoField(primary_key=True)
    description = models.TextField()
    function_header = models.CharField(max_length=48)
    attempts = models.SmallIntegerField(default=3, help_text="Use -1 for unlimited attempts")
    is_hidden = models.BooleanField(default=False, editable=False)


class Doctest(models.Model):
    uid = models.AutoField(primary_key=True)
    input = models.CharField(max_length=64)
    output = models.CharField(max_length=64)
    is_hidden = models.BooleanField(default=False)
    activity = models.ForeignKey(to=Activity, related_name="doctests", on_delete=models.CASCADE)


class Snippet(models.Model):
    uid = models.AutoField(primary_key=True)
    code = models.TextField()
    activity = models.ForeignKey(to=Activity, related_name="snippets", on_delete=models.CASCADE)
    programmer = models.ForeignKey(to=Account, related_name="snippets", on_delete=models.SET_NULL, null=True)
